fn main() {
  println!("Control Flow!!");
  if_statement();
  loops();
  match_statement(0);
  match_statement(1);
  match_statement(200);
  match_statement(15);
}

fn if_statement()
{
  let temperature = 100;

  if temperature > 95
  {
    println!("It is hot.");
  } else if temperature < 65
  {
    println!("Temperature is cold.");
  } else {
    println!("Temperature is acceptable.")
  }

  let day = if temperature > 70 { "Sunny" } else { "Cloudy" };
  println!("Today it is {} outside.", day);

  println!("Today is {}.",
           if temperature > 80 { if temperature > 95 { "very bad" } else { "bad" } } else { "good" });
}

fn loops()
{
  let mut x = 1;
  println!("\nx is {}", x);
  while x < 1000
  {
    x *= 2;

    // continue statement
    if x == 64
    {
      println!("skipping 64");
      continue;
    }

    println!("x is now {}", x);
  }

  // loop continues forever until break.
  let mut y = 1;
  println!("\ny is {}", y);
  loop // like while true
  {
    y *= 2;
    println!("y is now {}", y);

    if y > 100
    {
      break;
    }
  }

  println!();
  for z in 1..11 // [1, 11); 11 is excluded.
  {
    if z == 7
    {
      println!("skipping 7!");
      continue;
    } else if z == 10
    {
      println!("z is done.");
      break;
    }
    println!("z = {}", z);
  }

  println!();
  // enumerate values
  for (i, val) in (30..36).enumerate()
  {
    println!("i is {}; val is {}", i, val);
  }
}

fn match_statement(op_code:u8)
{
  println!("\nPattern matching!!");

  let op = match op_code
  {
    1 => "+",
    2 => "-",
    3 => "*",
    4 => "/",
    0..=124 => "undefined", // '..=' is inclusive [first, last]
                            // where just '..' is exclusive [first, last)
    // 125..200 => "error",    // cannot use '..' in match statement.
    _ => "invalid"  // catch all
  };

  println!("The op with code {} is '{}'", op_code, op);
}
