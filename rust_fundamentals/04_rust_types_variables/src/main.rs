use std::mem; // used to get variable bytes

// module definition
mod stack_heap;

// globals can be defined as constants, inlined wherever used (no memory address)
const GLOBAL10:u8 = 10;

// globals can also be static: exists once in memory, not reallocated each time.
static STATIC100:u8 = 100;

// mutable statics can exist too, but can only be modified from an `unsafe { <code> }` block.
static mut STATICMUTABLE:u8 = 41;

fn main() {
  datatypes();
  operators();
  scope();
  globals();
  memory();
  stack_heap::stack_and_heap();
}

// Datatype examples
fn datatypes()
{
  println!("\n\nDatatype Examples!!");

  let a:u8 = 123; // name:type = value; u8 ==> unsigned, 8 bits
  let b:i8 = -5;

  // println! macro
  println!("a = {}", a);
  println!("b = {}", b);

  // cannot mutate variables by default. have to declare mut
  let mut c:i16 = 2000;
  println!("c = {}", c);

  println!("mutating!");
  c *= -1;

  println!("c = {}", c);


  // automatic type deduction
  let mut d = 123; // i32 by default
  println!("d = {}, sizeof d = {} bytes", d, mem::size_of_val(&d));

  println!("mutating!");
  d *= -1;

  println!("d = {}", d);

  // dtypes: i, u, 8, 16, 32, 64
  let s:isize = 123; // isize / usize default word-sized types
  // 8 / 16 / 64 on respective n-bit os
  let size_of_s = mem::size_of_val(&s);
  println!("s = {}; size of s is is {} bytes; this is running on \
            a {}-bit OS.", s, size_of_s, size_of_s *8);

  let e:char = 'e';  //default size is largest unicode set - 4 bytes.
  println!("e is the character: {}, size of e is {} bytes.",
           e,
           mem::size_of_val(&e));

  // floats:
  // ieee-754 compliant
  let f = 5.5; // Real default is 8 bytes, 64-bits, f64
  println!("f is a real-valued number: {}, size of f is {} bytes.",
           f,
           mem::size_of_val(&f));


  // booleans
  let g = false; // bool, 1 byte
  println!("g = {}, size of g is {} byte.", g, mem::size_of_val(&g));
}

// rust operator examples.
fn operators()
{
  println!("\n\nOperator examples!!");

  // arithmetic
  let mut a = 1+2*3; // pemdas: *, /, +, -
  println!("a = 1 + 2 * 3 = {}", a);

  // incriment / decriment:
  a = a + 1;  // no ++ or --
  a -= 2;

  // modulo
  println!("remainder of {} / {} = {}", 8, 3, 8 % 3);

  // power
  let cubed = i32::pow(a, 3);
  println!("a^3 = {}", cubed);

  const B:f64 = 3.3;
  let cubed_integral = f64::powi(B, 3); // integer power, 3.3*3.3*3.3
  let pi:f64 = std::f64::consts::PI;
  let cubed_nonintegral = f64::powf(B, pi); // non-integral e.g. taylor

  println!("B = {}", B);
  println!("B^3 = {}", cubed_integral);
  println!("B^Pi = {}", cubed_nonintegral);

  // bitwise operators (integers only)
  // operators: | OR, & AND, ^ XOR, !NOR, << >> shifts
  let c = 1 | 2; // 3
  println!("c = 1 | 2 = {}", c);
  let two_to_the_four = 2 << 4;
  println!("2^4 = {}", two_to_the_four);

  // logical <, >, <=, >=, ==
  let c_lt_four = c < 4;
  println!("Is c less than four? {}", c_lt_four);
}

// scope examples
fn scope()
{
  println!("\n\nScope examples!!");

  let a = 123;

  // new scope
  {
    println!("a from outer scope is {}.", a);

    let b = 234;
    println!("b is {}", b);

    // redefine a for inner scope
    let a = 456;
    println!("a from inner scope is {}.", a);

  }

  println!("outer a = {}", a);

  // variable redefinition overrides previous declaration
  let a = 789;
  println!("redefined outer a = {}", a);

  // b is no longer present; this does not compile:
  // println!("b = {}", b);

}


fn globals()
{
  println!("\n\nGlobal variables!!");
  println!("GLOBAL10 is {}", GLOBAL10); // inlined integer
  println!("STATIC100 is {}", STATIC100); // dereferenced integer

  //  This does not compile, as static mutable can only be accessed through `unsafe {...}`
  // println!("STATICMUTABLE is unsafe {}", STATICMUTABLE);

  unsafe
  {
    println!("STATICMUTABLE is unsafe: {}", STATICMUTABLE);
    STATICMUTABLE += 1;
    println!("STATICMUTABLE is modifiable and unsafe: {}", STATICMUTABLE);
  }
}


fn inc(x:i32) -> i32 {return x + 1;}

fn memory()
{
  println!("\n\nMemory (Stack and Heap)!!");
  let x = 5; // stack allocation, beware using too much stack.
  println!("x is {}", x);

  // function arguments live on the stack
  let x_plus_one = inc(x);
  println!("(inc x) is {}", x_plus_one);

  // allocate on heap
  let y = Box::new(10);
  println!("y is {}; *y is {}", y, *y); // both resolve to '10'

  let x_ptr = &x;
  println!("x address is {}, x is {}", x_ptr, *x_ptr); // both resolve to '5'

  // vec<i32>
  let v = vec![4,5,6]; // heap allocation, address stored to v
  println!("v[0] is {}", v[0]);
}