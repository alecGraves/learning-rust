#![allow(dead_code)] // allow dead code in this source file

// measure variable sizes
use std::mem;

struct Point
{
  x: f64,
  y: f64,
}

fn origin() -> Point
{
  return Point { x: 0.0, y: 0.0 };
}


// pub allows external usage, like exports {}...
pub fn stack_and_heap()
{
  let o1 = origin();
  let o2 = Box::new(origin());

  println!("o1 (stack) takes {} bytes.", mem::size_of_val(&o1)); // 16 bytes total (no overhead)
  println!("o2 (heap) takes {} bytes.", mem::size_of_val(&o2)); // 8 bytes on stack (64 bit os)

  // how to transfer boxed value back onto stack
  let p1 = *o2; // transfer o2 to stack
  println!("p1 is ({}, {})", p1.x, p1.y);
}