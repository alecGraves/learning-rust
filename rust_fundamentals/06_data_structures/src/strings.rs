pub fn strings()
{
  println!("\n\nStringy!!");

  // a valid sequence of utf-8 characters - difficult to index as bytes.
  let s:&'static str = "why howdy there!"; // &str = string slice,
  // cannot assign new value: s = "new value"
  // cannot manipulate individual chars: let h = s[0];

  for c in s.chars().rev() // s.chars gives character array from str, rev reverses the array.
  {
    println!("{}", c);
  }

  if let Some(first_character) = s.chars().nth(0)
  {
    println!("the first letter is {}!", first_character);
  }

  // heap-stored strings!
  // String
  let mut alphabet = String::new();
  for i in 0..26
  {
    alphabet.push((('a' as u8) + i) as char);
    if i < 25
    {
      alphabet.push(',' as char);
    }
  }
  println!("The alphabet is: {:?}", alphabet);

  // String to str, &alphabet derefs String
  let _u:&str = &alphabet;

  // concatentation of strings (String + str)
  let hello = String::from("hello");
  let world = " world ".to_string();
  let exc = "!! ";
  let my_string = hello + &world + exc + &alphabet; // note & to dereference Strings
  println!("my_string is {}.", my_string);
  println!("all done, {}!!", my_string.replace("hello", "goodbye"));
}