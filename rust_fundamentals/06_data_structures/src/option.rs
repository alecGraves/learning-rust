
pub fn option()
{
  println!("\n\nOptional Option<T>!!");

  let a = 0.7;
  let b = 0.4;

  let result:Option<f64> = if a + b <= 1.0 { Some(a + b) } else { None };

  println!("This value is optional: {:?}", result);

  match result
  {
    Some(z) => println!("a + b is less than or equal to one: {}", z),
    None => println!("Error: a + b is greater than one.")
  }

  // if-let or while-let
  if let Some(_z) = result
  {
    // only runs if optional is not None
    println!("The optional is!");
  }
}
