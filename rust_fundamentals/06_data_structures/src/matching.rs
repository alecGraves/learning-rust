// convert value to roman numerals
fn romanify(x:u16) -> String
{
  return match x
  {
    _ if x >= 1000 => "M".to_string() + &romanify(x-1000),
    _ if x >= 900 => "CM".to_string() + &romanify(x-900),
    _ if x >= 500 => "D".to_string() + &romanify(x-500),
    _ if x >= 400 => "CD".to_string() + &romanify(x-400),
    _ if x >= 100 => "C".to_string() + &romanify(x-100),
    _ if x >= 90 => "XC".to_string() + &romanify(x-90),
    _ if x >= 50 => "L".to_string() + &romanify(x-50),
    _ if x >= 40 => "XL".to_string() + &romanify(x-40),
    _ if x >= 10 => "X".to_string() + &romanify(x-10),
    _ if x >= 9 => "IX".to_string() + &romanify(x-9),
    _ if x >= 5 => "V".to_string() + &romanify(x-5),
    _ if x == 4 => "IV".to_string(),
    _ if x >= 1 => "I".to_string() + &romanify(x-1),
    _ => "".to_string() // zero, could be 'nulla' but not really part of Roman numerals
  };
}

fn print_roman(x:u16)
{
  println!("{} is {}", x, romanify(x));
}

pub fn pattern_matching()
{
  println!("\n\nMatching Patterns!!");

  for x in 1..12
  {
    println!("{}", romanify(x));
  }

  // https://en.wikipedia.org/wiki/Roman_numerals

  print_roman(1000);
  print_roman(3000);
  print_roman(700);
  print_roman(800);
  print_roman(900);
  print_roman(90);
  print_roman(9);
  print_roman(44);
  print_roman(1900);
  print_roman(1912);
  print_roman(2021);
}