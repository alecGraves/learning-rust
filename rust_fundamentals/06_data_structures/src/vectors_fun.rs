pub fn vectors()
{
  println!("\n\nVectors!!");
  let mut v = Vec::new();
  for i in 1..4
  {
    v.push(i);
  }
  println!("My vector is {:?}.", v);

  v.push(42);
  println!("My vector is resizable: {:?}", v);

  // This des not compile, idx address must be usize:
  // let idx_bad:i32 = 0;
  // println!("a[0] is {}", v[idx_bad]);
  let idx:usize = 0;
  println!("v[{}] = {}", idx, v[idx]);

  // out-of-bounds causes kernel panic(crash)
  // let idx_out_of_bounds:usize = v.len()+1;
  // println!("v[{}] = {}", idx_out_of_bounds, v[idx_out_of_bounds]);

  // safe accessor:
  match v.get(42)
  {
    Some(x) => println!("v[42] is {}.", x),
    None => println!("v[42] does not exist (out of bounds).")
  }

  // iterating a vector:
  for x in &v // need &v because v would move data, making it inaccessible.
  {
    println!("{}", x);
  }

  v.push(42);
  println!("vector is {:?}", v);

  // this is an optional, returning None if vector is empty.
  if let Some(last) = v.pop()
  {
    println!("popped {:?}, vector is now {:?}", last, v);
  }

  // loop, popping vector elements until no more:
  while let Some(last) = v.pop()
  {
    println!("Popped {}", last)
  }

}