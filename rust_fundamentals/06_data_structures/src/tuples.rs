
// computes sum and average of two values, returns tuple.
fn sum_and_average(x:f64, y:f64) -> (f64, f64)
{
  let sum = x + y;
  let avg = sum / 2.0;
  return (sum, avg);
}

pub fn tuples()
{
  println!("\n\nTuples!!");
  let a = 3.0;
  let b = 4.0;

  let sum_average = sum_and_average(a, b);
  println!("sum_and_average of {} and {} = {:?}", a, b, sum_average);
  println!("{0} + {1} = {2}; {0} + {1} = {3}", a, b, sum_average.0, sum_average.1);

  // destructuring
  let (sum, avg) = sum_average;
  println!("The sum is {}; the mean is {}.", sum, avg);

  let sum_average_2 = sum_and_average(12.3, 32.8);
  let combined_tuples = (sum_average, sum_average_2);
  println!("Behold my combined tuples!! {:?}", combined_tuples);
  println!("The last element is {}", (combined_tuples.1).1);

  let ((w, x), (y, z)) = combined_tuples;
  println!("w is {}, x is {}, y is {}, and z is {}.", w, x, y, z);

  let multitype_tuple = (true, 1, 1.5, "!!");
  println!("Behold my multi-type tuple: {:?}", multitype_tuple)
}