fn eat_slice(slice: &[i32])
{
  // arg : &[i32] -> we are 'borrowing' a chunk of i32
  println!("Im eating: {:?}... of length {}!", slice, slice.len());
}

fn poison_slice(slice: &mut [i32])
{
  // arg : &mut [i32] -> we are 'borrowing' a chunk of i32 that is mutable
  match slice.get_mut(0)
  {
    Some(x) => *x = 13,
    None => println!("Cannot poison an empty slice!")
  }
  println!("The slice has been poisoned! See: {:?}", slice);
}

pub fn slices()
{
  println!("\n\nSlices!!");
  let mut data = [1, 2, 3, 4, 42];
  eat_slice(&data[1..4]); // this is notation for a slice of elements at idx [1, 4) exclusive
  poison_slice(&mut data[2..4]); // must pass mutable reference to function with mut args
  poison_slice(&mut data); // you can use the entire array
  println!("The data is now {:?}", data)
}