struct Point
{
  x: f64,
  y: f64,
}

struct Line
{
  start: Point,
  end: Point
}

pub fn data_structures()
{
  let p1 = Point { x: 0.4, y: 23.3 };
  println!("point p1 is ({}, {}).", p1.x, p1.y);

  let p2 = Point { x: 5.7, y: 4.9 };
  let l1 = Line { start: p1, end: p2 };

  println!("line start is ({}, {}).", l1.start.x, l1.start.y);
  println!("line end is ({}, {}).", l1.end.x, l1.end.y);
}
