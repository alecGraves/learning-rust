#![allow(dead_code)]

// template< typename T >... it has been fixed.

struct Point<T>
{
  x: T,
  y: T,
  z: T
}

struct Line<T>
{
  start: Point<T>,
  end: Point<T>
}

pub fn generics()
{
  println!("\n\nGenerics!!");

  let a:Point<i32> = Point {x: 0, y: 0, z: 0 };
  let b:Point<f64> = Point {x: 0.0, y: 0.0, z: 6f64 }; // note nf64 for type notation

  let _l = Line {start: Point {x: 3.0, y: 4.0, z: 5.0}, end: Point {x: 1.0, y: 2.0, z: 3.0}};

  println!("a.z is i32 {:?},\nb.z is f64 {:?}", a.z, b.z);

  // let my_line = Line { start: a, end: b }; // cannot combine points of two different types.
}
