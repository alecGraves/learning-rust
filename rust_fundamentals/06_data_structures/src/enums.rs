#[allow(dead_code)]

enum Color
{
  Pink,
  Cyan,
  Orange,
  LABColor(u32, u32, u32),
  HSVColor{hue: u32, saturation: u32, value:u32}
}

fn color_test(c: Color)
{
  match c
  {
    Color::Pink => println!("pink!"),
    Color::Cyan => println!("cyan!"),
    Color::LABColor(0, _, _) => println!("black!"), // a color with 0 'lightness'
    Color::HSVColor{hue: 4294967295, ..} => println!("super-saturated!"),  // .. = ..., any other
    _ => () // _ catches all remaining, prints nothing
  }
}

pub fn enums()
{
  println!("\n\nEnums!!");
  let a:Color = Color::Pink;

  println!("Color a is...");
  color_test(a);

  let b:Color = Color::LABColor(0, 10, 45);
  println!("Color b is... ");
  color_test(b);
}
