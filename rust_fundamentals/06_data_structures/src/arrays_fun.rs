use std::mem;

pub fn arrays()
{
  println!("\n\nArrays!!");
  // let mut a:i32 // single variable
  let mut a:[u32; 5] = [1, 2, 3, 4, 5]; // array variable, 5 elements long

  println!("a has {} elements! The first element is {}.", a.len(), a[0]);

  a[0] += 41;
  println!("a is mutable! The first element is now {}.", a[0]);

  println!("I can print all of a! See: {:?}", a);

  // array comparison, must have arrays of same length, type
  if a != [1, 2, 3, 4, 5]
  {
    println!("A has been modified!");
  }

  // set array to const '1', type u16
  let b = [1u16;10];
  for i in 0..b.len()
  {
    println!("b[{}] is {}!", i, b[i]);
  }

  println!("b takes {} bytes!", mem::size_of_val(&b));

  // multi-arrays
  let matrix:[[f32;3]; 2] =
    [
      [1.0, 0.0, 0.0],
      [0.0, 1.0, 0.0],
    ];

  // multi-array iterator:
  println!("Look, a matrix! {:?}", matrix);
  for i in 0..matrix.len()
  {
    for j in 0..matrix[1].len()
    {
      if i == j
      {
        println!("Diagonal: {}", matrix[i][j]);
      }
    }
  }
}
