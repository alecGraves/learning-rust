mod data_structures;
mod enums;
mod option;
mod arrays_fun;
mod vectors_fun;
mod tuples;
mod slicing;
mod strings;
mod matching;
mod generics;

fn main() {
  println!("Hello, world!");
  data_structures::data_structures();
  enums::enums();
  option::option();
  arrays_fun::arrays();
  vectors_fun::vectors();
  slicing::slices();
  strings::strings();
  tuples::tuples();
  matching::pattern_matching();
  generics::generics();
}


